var g_fs = require('fs');


module.exports = function(opts) {
	console.log("[my_logger.js] opts = " + JSON.stringify(opts));	
	opts = opts || {};
	var logPath = opts.logPath || "log/events.log";

	return {
		writeStream: function() {
			var d = new Date();
			var month = this._zeroPadding(d.getMonth()+1);
			var yyyymm = d.getFullYear().toString() + month.toString();
			var wLogPath = logPath + "." + yyyymm;

			return g_fs.createWriteStream(wLogPath, {flags:'a'});
		},
		getTimeStamp: function() {
			var ts = new Date().getTime();
			return "[" + this._toHumanReadableDateTime(ts) + "]";
		},
		_zeroPadding: function(val) {
			if(val < 10) {val = "0" + val;} return val;
		},
		_toHumanReadableDateTime: function(ts) {
			var d = new Date(ts);
			var month = this._zeroPadding(d.getMonth()+1);
			var date = this._zeroPadding(d.getDate());
			var hour = this._zeroPadding(d.getHours());
			var min = this._zeroPadding(d.getMinutes());
			var sec = this._zeroPadding(d.getSeconds());
			return d.getFullYear() + "-" + month + "-" + date + " " + hour + ":" + min + ":" + sec;
		},

		format: function(level, tag, msg) {
			var title = tag ? " ["+tag+"] " : " ";
			var line = this.getTimeStamp() + title + msg;
			switch(level) {
				case "debug": line = "D." + line; break;
				case "info":  line = "I." + line; break;
				case "warn":  line = "W." + line; break;
				case "error": line = "E." + line; break;
				case "fatal": line = "F." + line; break;
			}
			return line;
		},
		d: function(msg, tag) {
			var line = this.format("debug", tag, msg);
			this.writeStream().write(line+"\n");
			console.log(line);
		},
		i: function(msg, tag) {
			var line = this.format("info", tag, msg);
			this.writeStream().write(line+"\n");
			console.log(line);
		},
		w: function(msg, tag) {
			var line = this.format("warn", tag, msg);
			this.writeStream().write(line+"\n");
			console.log(line);
		},
		e: function(msg, tag) {
			var line = this.format("error", tag, msg);
			this.writeStream().write(line+"\n");
			console.log(line);
		},
		f: function(msg, tag) {
			var line = this.format("fatal", tag, msg);
			this.writeStream().write(line+"\n");
			console.log(line);
		}
	}
}

// exports.d = d;
// exports.i = i;
// exports.w = w;
// exports.e = e;
// exports.f = f;

