var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

// ==============================================
// Purpose: fix : Same Origin Policy
// Install:
// Reference:
// ==============================================
var allowProdOrigins = 'www.foo.com:*';
var allowDevOrigins = 'localhost:* 127.0.0.1:*';
io.set('origins', allowProdOrigins + " " + allowDevOrigins);

var MyLogger = require("./my_logger")({
  logPath: "log/events.log"
});


var g_data = {
  usernames: new Set(),
  addUser: function(username) {
    usernames.add(username);
  }
};

io.on('connection', function(socket) {
  MyLogger.i('a user connected');

  let username = null;

  // fix: if server restart
  // if(socket.handshake) {
  //   roomId = socket.handshake.query.roomId;
  //   if(roomId) {
  //     MyLogger.i(' + user join, socket.handshake.query.roomId=' + roomId);
  //     socket.join(roomId);
  //   }
  // }

  socket.on('join-a-room', function(_username) {
    MyLogger.i("@join-a-room: username=" + username);
    username = _username;

    g_data.addUser(username);
    // socket.broadcast.emit('join-a-room', username);
    io.emit('join-a-room', username);
  });
  socket.on('message', function(data) {
    MyLogger.i("@message: data=" + roomId);
    io.emit('message', username);
  });

  socket.on('disconnect', function(){
    socket.leave();
    //io.emit('leave-a-room', username);
    MyLogger.i('A user disconnected');
  });
})

server.listen(5000, function(err) {
  // nginx proxy_pass from 8080 -> 5000
  MyLogger.i('*******************************************');
  MyLogger.i('Socket.IO subscriber');
  MyLogger.i(' + listening on *:5000');
  MyLogger.i('*******************************************');
});
