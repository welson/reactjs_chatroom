import Logger from '../util/Logger.js';
import app, { sys } from '../assets/application.js';
import createAction from './createAction.js';

export function FooAction() {
    var ret = createAction("foo", {});
    Logger.d("FooAction", JSON.stringify(ret));
    return ret;
}

export default FooAction;
