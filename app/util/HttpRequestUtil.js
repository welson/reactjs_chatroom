import app from '../assets/application.js';
import Logger from '../util/Logger.js';

const HttpRequestUtil = {
  API_URL: function(path) {
    if(path.startsWith("http") || path.startsWith("ftp")) {
      return path;
    } else {
      return app.constants.host + "/tipga" + path;
    }
  },
  createHeader: function() {
    var h = new Headers();
    h.append('Accept', 'application/json');
    h.append('Content-Type', 'application/json');
    return h;
  },
  get: function(params) {
    params.method = 'GET';
    return this.request(params);
  },
  post: function(params) {
    params.method = 'POST';
    return this.request(params);
  },
  request: function(params) {
    params = params || {};
    var method = (params.method || "").toUpperCase();
    var url = params.url;
    var postData = params.data;
    var beforeSend = params.beforeSend;
    var successCB = params.success;
    var errorCB = params.error;
    var completeCB = params.complete;
    var timeout = params.timeout || 120000;

    var pathName = url.substring(url.lastIndexOf('/s/') + 2);

    var headers = this.createHeader();

    var req = null;
    var reqInit = {
      //mode: 'cors',
      //cache: 'reload', // 'default'
      headers: headers,
    };
    switch(method) {
      case 'GET':
        reqInit.method = 'GET';
        break;
      case 'POST':
        reqInit.method = 'POST';
        reqInit.body = postData;
        Logger.d("## POST " + pathName + ", postData=", postData);
        break;
      default:
        reqInit.method = method;
        break;
    }

    req = new Request(url, reqInit);

    if(beforeSend) {
        beforeSend(params);
    }

    var timeoutId = -1;
    var ret = new Promise((resolve, reject) => {
      fetch(req).then(function(response) {
        if(timeoutId > -1) {
          clearTimeout(timeoutId);
        }
        // 處理成 json 再 return
        return response.json();
      })
      .then(function(json) {
        try { // for promise debug
          if(method == 'POST') {
            Logger.d("## POST " + pathName + ", resp=", json);
          }


          if(successCB){
            successCB(json);
          }

          if(completeCB) {
            completeCB(json);
          }
        } catch(e) {
          console.error(e);
        }
      })
      .catch(function(err) {
        if(errorCB){
          errorCB(err);
        }
      })
      .done();

      if(timeout > 0) {
        timeoutId = setTimeout(function() {
          let err = new Error('Http Request Timeout Out!!');
          reject.bind(null, err);
          if(errorCB) {
            errorCB(err);
          }
        }, timeout);
      }
    });

    return ret;
  }
};

export default HttpRequestUtil;
