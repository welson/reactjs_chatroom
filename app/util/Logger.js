const Logger = {
  d: function(title, msg) {
    this._print("D", title, msg);
  },
  i: function(title, msg) {
    this._print("I", title, msg);
  },
  w: function(title, msg) {
    this._print("W", title, msg);
  },
  e: function(title, msg) {
    this._print("E", title, msg);
  },
  f: function(title, msg) {
    this._print("F", title, msg);
  },

  _print: function(prefix, title, msg) {
    try {
      var ts = new Date().getTime();
      var dt = this.parseDigit(ts);
  	  var datetime = dt.yyyy + "-" + dt.mm + "-" + dt.dd + " " + dt.HH + ":" + dt.MM + ":" + dt.SS;

      if(typeof(title) == "string" && typeof(msg) == "string") {
        console.log(prefix + ".[" + datetime + "] " + title + " " + msg);
      } else if(typeof(title) == "string" && typeof(msg) == "object") {
        console.log(prefix + ".[" + datetime + "] " + title, msg);
        // console.log(msg);
      } else {
        title = title || "";
        msg = msg || "";
        console.log(prefix + ".[" + datetime + "] " + title + " " + msg);
      }
    } catch(e) {
      console.error(e.message);
    }
  },
  parseDigit: function(ts) {
		var d = new Date(ts);
		var month = this.zeroPadding(d.getMonth()+1);
		var date = this.zeroPadding(d.getDate());
		var hour = this.zeroPadding(d.getHours());
		var min = this.zeroPadding(d.getMinutes());
		var sec = this.zeroPadding(d.getSeconds());
		//return d.getFullYear() + "-" + month + "-" + date + " " + hour + ":" + min + ":" + sec;
		var ret = {
			yyyy: d.getFullYear(),
			mm: month,
			dd: date,
			HH: hour,
			MM: min,
			SS: sec
		};
		return ret;
	},
  zeroPadding: function(val) {
  	if(val < 10) {val = "0" + val;} return val;
  }
};

export default Logger;
