import { combineReducers } from 'redux';
import chatRoomReducer from './chatRoomReducer.js';
// ... other reducers

export default combineReducers({
  chatRoomInfo: chatRoomReducer,
  // ... other reducers
});
