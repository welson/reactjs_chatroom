import app from '../assets/application.js';
import Logger from '../util/Logger.js';

export const initialState = {
  usernames: [],
  msgList: [],
};
export default function chatRoomReducer (state = initialState, action = {}) {
    var ret = null;
    var payload = action.payload || {};

    Logger.i("chatRoomReducer start : action=", action);
    switch (action.type) {
        case app.constants.actionTypes.RECEIVE_MSG:
        case app.constants.actionTypes.SEND_MSG:
            // state.user = payload.isLogin;
            // state.nickname = payload.nickname || "";
            // state.token = payload.token || "";
            // ret = { ...state }; // means: duplicate a json with all fields of state
            ret = state; // means: duplicate a json with all fields of state
            break;
        default:
            ret = state;
            break;
    }

    Logger.i("chatRoomReducer done : ret=", ret);
    return ret;
}
