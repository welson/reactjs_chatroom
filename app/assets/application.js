
const app_constants = {
  appName: "foo",
  host: "https://www.foo.com",
  hostname: 'www.foo.com',
  cookies: {
    ACCOUNT_NICKNAME: "foo.account.nickname",
    ACCOUNT_TOKEN: "foo.account.token",
    ACCOUNT_LANG: "foo.account.language"
  },
  actionTypes: {
    INIT_APP: 'init-app',
    SEND_MSG: 'send-msg',
    RECEIVE_MSG: 'receive-msg',
    LOGIN: 'login',
    LOGOUT: 'logout',
  },
};

const app_result_code = {
  // success/fail
  SUCCESS: 200,
  FAIL: 500,
  INPUT_ERR: 400,
};

const app_defaults = {
  language: "zh-TW",
};

export { app_constants, app_result_code, app_defaults };

const app = {
  constants: app_constants,
  resultCode: app_result_code,
  defaults: app_defaults,
};
export default app;
