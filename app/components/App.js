import React, { Component } from 'react';
import { connect } from 'react-redux';
import app from '../assets/application.js'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this._init();
  }
  _init() {
     g_socket.on('message', (msg) => {
       this.props.newMessage({user: 'test_user', message: inboundMessage})
     });
  }
  render() {
    return (
      <div>
        <h1>Hello, World!</h1>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return state;
}

function mapDispatchToProps(dispatch) {

}

export default connect(mapStateToProps, mapDispatchToProps)(App);
