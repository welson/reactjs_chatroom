
class ChatRoom extends React.Component {
  render() {
    return (
      <div id="msgList">
        <ul id="messages"></ul>
      </div>

      <div style="background:#A8DAEF; position:fixed; bottom:0px; width:100%; padding:5px;">
        <form action="">
          <div class="form-group">
            <input id="username" placeholder="Username" class="col-xs-2">
            <input id="m" class="col-xs-8" />
            <button id="sendMsg" class="btn btn-default col-xs-2">Send</button>
          </div>
        </form>
      </div>
    )
  }
}
